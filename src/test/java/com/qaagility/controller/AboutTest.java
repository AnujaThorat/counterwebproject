package com.qaagility.controller;

import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.ui.ModelMap;

public class AboutTest {

    @Test
    public void testDesc() {
        // Arrange
        About about = new About();

        // Act
        String result = about.desc();

        // Assert
        String expected = "This application was copied from somewhere, sorry but I cannot give the details and the proper copyright notice. Please don't tell the police!";
        assertEquals("The description was not as expected",expected, result);
    }
}