package com.qaagility.controller;

import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.ui.ModelMap;

public class CntTest {

    @Test
    public void testDivide() {
        // Arrange
        Cnt cnt = new Cnt();

        // Act
        int result = cnt.divide(10, 2);

        // Assert
       assertEquals("div", 5, result);
    }

    @Test
    public void testDivideByZero() {
        // Arrange
        Cnt cnt = new Cnt();

        // Act
        int result = cnt.divide(10, 0);

        // Assert
        assertEquals("div", Integer.MAX_VALUE, result);
    }
}
